1. Buat Database
CREATE DATABASE MYSHOP;

2.Membuat Table di dalam DATABASE :
 a. Table Users
    - CREATE TABLE users( id integer AUTO_INCREMENT PRIMARY KEY, name varchar(255) NOT NULL,
      email varchar(225) NOT NULL, pasword varchar(225) NOT NULL );
 b. Table Categories
    - CREATE TABLE categories( id integer PRIMARY KEY AUTO_INCREMENT, name varchar(225) NOT NULL );
 c. Table Items
    - CREATE TABLE items( id integer PRIMARY KEY AUTO_INCREMENT, name varchar(255) NOT NULL,
      description varchar(255) NOT NULL, price integer NOT NULL,
      stock integer NOT NULL, categories_id integer NOT NULL, FOREIGN KEY(categories_id) REFERENCES categories(id) );
3. Memasukan data ke dalam tabel :
  a. Users :
   - INSERT INTO users (name,email,pasword) VALUES ('john doe','john doe.com' , 'john123');
   - INSERT INTO users (name,email,pasword) VALUES ('jane doe','jane@doe.com','jenita123');
  b. categories :
   - INSERT INTO categories (name) VALUES ('gadget'),('cloth'),('men'),('women'),('branded');
  c. items :
   - INSERT INTO items (name,description,price,stock,categories_id)
     VALUES ('sumsang b50','hp keren dari merk sumsang',4000000,100,1);
   - INSERT INTO items (name,description,price,stock,categories_id)
     VALUES ('uniklooh','baju keren dari brand ternama',500000,50,2);
   - INSERT INTO items (name,description,price,stock,categories_id)
     VALUES ('IMHO Watch','jam tangan anak yang jujur banget',2000000,10,1);

4. a. SELECT `id`, `name`, `email` FROM `users`;
   b. - SELECT * FROM `items` WHERE price > 1000000;
      - SELECT * FROM `items` WHERE name like 'uniklo%';
   c. 
